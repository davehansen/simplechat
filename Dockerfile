FROM ubuntu:16.10

# Install.
RUN \
  apt-get update && \
  apt-get -y upgrade && \
  apt-get install -y python python-pip nginx gunicorn supervisor libmysqlclient-dev git

RUN git clone https://davehansen@bitbucket.org/davehansen/simplechat.git /opt/simplechat
COPY simplechat/app.cfg /opt/simplechat/simplechat/app.cfg

RUN pip install -e /opt/simplechat

# Setup nginx
RUN rm /etc/nginx/sites-enabled/default
COPY ./etc/flask.conf /etc/nginx/sites-available/

RUN ln -s /etc/nginx/sites-available/flask.conf /etc/nginx/sites-enabled/flask.conf
RUN echo "daemon off;" >> /etc/nginx/nginx.conf

# Setup supervisord
RUN mkdir -p /var/log/supervisor
COPY ./etc/supervisord.conf /etc/supervisor/conf.d/supervisord.conf
COPY ./etc/gunicorn.conf /etc/supervisor/conf.d/gunicorn.conf

# Start processes
CMD ["/usr/bin/supervisord"]
