# Simplechat

Simple, HTTP-based chat server. Allows sending and retrieving of messages, along with simple dashboard for monitoring recent chat activity.

**Current URL**: `http://simplechat-demo-1379223217.us-east-1.elb.amazonaws.com:2428/dashboard/`

## Endpoints

The chat server contains two modules; chat api endpoints and static files served to display the dashboard front-end.

To view the dashboard, go to the direct url:

`/dashboard/`: loads the server dashboard

Current API endpoints for message handling and analytics are:

`/api/v1/chat/send`: send new message to chat server; see JSON packet structure below

`/api/v1/chat/history`: retrieve messages from chat server at a given point in time; see JSON packet structure below

`/api/v1/info/hourly`: sum of messages sent in the past hour

`/api/v1/info/daily`: sum of messages sent in the past day

`/api/v1/info/users`: sum of all known senders


## Infrastructure & Environment
* AWS RDS *[persistant database]*
* AWS EC2 *[cloud-hosted server]*
* Python w/ Flask
* Served with *gunicorn* and *nginx*
* [rdash-angular fork](https://bitbucket.org/davehansen/rdash-angular/src) for AngularJS-based dashboard

## Development

Recommended that all local dev be ran and tested using Docker. This will provide a full mock integration. This is especially helpful with the nginx layer for avoiding CORS annoyances. Updates to the dashboard must be made in the `rdash-angular` repo, build with gulp, and copied into the `static` dir in this repository's root.

## Protocol


#### Request
**send**: `{"sender":"Alice", "msg":"Hello Bob!", "client_time":1477084334000}`

**history**: `{"command":"history","client_time":1477084767,"since":1477084385996}`

#### Response
**send**: `{"sender":"Alice", "msg":"Hello Bob!", "client_time":1477084334000, "server_time":1477084385997}`

**history**: `[{"sender":"Alice", "msg":"Hello Bob!", "client_time":1477084334, "server_time":1477084385997}, {"sender":"Bob", "msg":"Hi Alice!", "client_time":1477084402, "server_time":1477084440783}]`
