import os

from flask import Flask
from flask import jsonify

from simplechat.models import db
from simplechat.app.api.v1.routes import API as api_v1


APP = Flask(__name__)

APP.config.DEBUG = os.environ.get('DEBUG', False)
APP.config.from_pyfile('app.cfg')
APP.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://%s:%s@%s:3306/simplechat' % (APP.config['MYSQL_USER'],
                                                                              APP.config['MYSQL_PASSWORD'],
                                                                              APP.config['MYSQL_HOST'])

db.init_app(APP)


@APP.route('/status')
def status_test():
    return jsonify('ok')

APP.register_blueprint(api_v1, url_prefix='/api/v1')

if __name__ == '__main__':
    APP.run(debug=True, port=8000)