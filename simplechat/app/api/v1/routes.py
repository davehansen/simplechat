import time

from flask import Blueprint
from flask import jsonify
from flask import request

from simplechat.models import ChatMessage

API = Blueprint('api_v1', __name__)


@API.route('/chat/send', methods=['POST'])
def send_chat_message():
    chat_message = ChatMessage(
        request.json['sender'],
        request.json['msg'],
        request.json['client_time']
    )
    chat_message.send()

    return jsonify({
        'sender': chat_message.sender,
        'msg': chat_message.message,
        'client_time': chat_message.client_time,
        'server_time': chat_message.server_time
    })


@API.route('/chat/history', methods=['POST'])
def get_chat_history():
    pending_messages = []
    if request.json['command'] == 'history' and request.json['since']:
        pending_messages = ChatMessage.get_messages(request.json['since'])

    return jsonify([message for message in pending_messages])


@API.route('/info/users', methods=['GET'])
def get_user_list():
    user_list = ChatMessage.user_list()
    return jsonify(dict(users=user_list))


@API.route('/info/hourly', methods=['GET'])
def get_last_hour_message_count():
    messages_last_hour = ChatMessage.sum_messages_since(int(time.time()) - 3600)
    return jsonify(hourly=messages_last_hour)


@API.route('/info/daily', methods=['GET'])
def get_24hour_message_count():
    messages_last_day = ChatMessage.sum_messages_since(int(time.time()) - 86400)
    return jsonify(daily=messages_last_day)
