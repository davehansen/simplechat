import time

from flask_sqlalchemy import SQLAlchemy
db = SQLAlchemy()


class ChatMessage(db.Model):
    __tablename__ = 'chat_log'

    id = db.Column(db.Integer, primary_key=True)
    sender = db.Column(db.String(64), nullable=False)
    message = db.Column(db.String(4096), nullable=False)
    client_time = db.Column(db.BigInteger, nullable=False)
    server_time = db.Column(db.BigInteger, nullable=False)

    def __init__(self, sender, message, client_time):
        self.sender = sender
        self.message = message
        self.client_time = client_time
        self.server_time = int(round(time.time() * 1000))

    # counts unique users present in chat log
    @classmethod
    def user_list(cls):
        senders = ChatMessage.query.filter(ChatMessage.sender != '').distinct(ChatMessage.sender)
        unique_senders = list(set([s.sender for s in senders]))     # idioum to find unique elements in list

        return len(unique_senders)

    # count messages sent between now and `since` timestamp
    @classmethod
    def sum_messages_since(cls, since):
        messages_since = ChatMessage.query.filter(ChatMessage.server_time > since)

        return messages_since.count()

    # generate list of messages sent between now and `since` timestamp
    @classmethod
    def get_messages(cls, since, row_limit=100):
        pending_messages = ChatMessage.query \
                                      .filter(ChatMessage.server_time > since) \
                                      .order_by(ChatMessage.server_time.desc()) \
                                      .limit(row_limit)

        for message in pending_messages:
            yield {
                'sender': message.sender,
                'msg': message.message,
                'client_time': message.client_time,
                'server_time': message.server_time
            }

    # apply row to database
    def send(self):
        db.session.add(self)
        db.session.commit()

