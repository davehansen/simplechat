#! /usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup
from setuptools import find_packages
import os
import os.path as op

HERE = op.abspath(op.dirname(__file__))
# requires pip install -e .[extras-name]


scripts = []
for root, dirs, files in os.walk(op.join(HERE, 'stats_api', 'bin')):
    for filename in files:
        if filename.endswith('.py') or filename.endswith('sh'):
            scripts.append(op.abspath(op.join(root, filename)))


setup(name='simplechat',
      version='0.0.1',
      author='Dave Hansen',
      author_email='dave@davehansen.org',
      url='http://dave-hansen.github.io/simplechat/',
      zip_safe=False,
      packages=find_packages(),
      scripts=scripts,
      long_description=open(op.join(HERE, 'README.md')).read(),
      install_requires=[
        'requests >= 2.13.0',
        'flask >= 0.12',
        'mysql-python >= 1.2.5', 
        'sqlalchemy >= 1.1.5',
        'flask-sqlalchemy >= 2.1',
        'gunicorn >= 19.6.0'
      ],
      include_package_data=True
)
